const Footer = () => {
	return (
		<footer className="container w-100">
			<div className="d-flex flex-wrap justify-content-between align-items-center py-3 opacity-50">
				<p className="col-md-4 col-lg-auto mb-0 small"><span className="text-muted">Powered by </span><span className="fw-bold">ReactJS</span></p>
				<p className="col-md-4 col-lg-auto mb-0 small"><span className="text-muted">Created by </span><span className="fw-bold">fikriomar16</span></p>
			</div>
		</footer>
	)
}
export default Footer