const FormInput = ({ type, id, placeholder, value, onChange, label, required = false, addClass, autoFocus = false }) => {
	return (
		<>
			<div className={ `form-floating ${addClass}` }>
				<input type={ type } className="form-control rounded-4" id={ id } placeholder={ placeholder } value={ value } onChange={ onChange } required={ required } autoFocus={ autoFocus } />
				<label htmlFor="name">{ label }</label>
			</div>
		</>
	)
}

export default FormInput