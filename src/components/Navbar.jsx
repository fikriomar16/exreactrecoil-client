import axios from "axios"
import { Link, NavLink, useLocation, useNavigate } from "react-router-dom"
import { useRecoilState, useSetRecoilState } from "recoil"
import logo from '../logo_home.svg'
import { authData, isAuth, toastState } from "../state"

const Navbar = () => {
	const [checkAuth, setCheckAuth] = useRecoilState(isAuth)
	const [authPerson, setAuthPerson] = useRecoilState(authData)
	const setToast = useSetRecoilState(toastState)

	const navigate = useNavigate()
	const location = useLocation()
	const Logout = async () => {
		try {
			await axios.post('/auth/logout')
			setCheckAuth(false)
			setAuthPerson(null)
			localStorage.removeItem('token')
			setToast({
				type: 'success',
				message: 'Logout Success'
			})
			navigate('/')
		} catch (error) {
			console.log(error)
		}
	}
	return (
		<>
			<nav className={ `navbar navbar-expand-lg rounded-bottom ${checkAuth ? ` sticky-top bg-light` : ``}` }>
				<div className="container">
					{
						location.key !== 'default' &&
						<>
							<button className="btn btn-danger btn-sm shadow-sm me-0 fw-bold opacity-75 h-100" onClick={
								() => {
									if (location.key !== 'default') navigate(-1)
								}
							} disabled={ location.key === 'default' } >
								<h6 className="my-auto">
									<i className="bi bi-chevron-double-left"></i>
								</h6>
							</button>
						</>
					}
					<Link className="navbar-brand ms-lg-5 ms-4 fw-bold transition3" to="/">
						<img src={ logo } alt="logo" loading="lazy" className="spin-logo-5 img-fluid transition3" height={ 40 } width={ 40 } />
						<span className="small d-none d-lg-inline">GALERIAsal</span>
					</Link>
					<button className="navbar-toggler border-0 shadow-none small" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
						<i className="bi bi-columns-gap small logo-shadow"></i>
					</button>
					<div className="collapse navbar-collapse" id="navbarNavAltMarkup">
						{
							checkAuth &&
							<>
								<div className="navbar-nav p-3 p-lg-1">
									<NavLink activeclassname="active" to="/exhibition" className="nav-link">Exhibition</NavLink>
									<NavLink activeclassname="active" to="/search" className="nav-link">Search People</NavLink>
									{
										(authPerson?.role === 'super' || authPerson?.role === 'admin') &&
										<>
											<NavLink activeclassname="active" to="/roles" className="nav-link">Roles</NavLink>
											<NavLink activeclassname="active" to="/users" className="nav-link">Users</NavLink>
										</>
									}
									<div className="d-none">
										<NavLink activeclassname="active" to="/super" className="nav-link">Super</NavLink>
										<NavLink activeclassname="active" to="/admin" className="nav-link">Admin</NavLink>
										<NavLink activeclassname="active" to="/moderator" className="nav-link">Moderator</NavLink>
									</div>
								</div>
							</>
						}
						<div className="navbar-nav p-3 p-lg-1 ms-auto">
							{
								checkAuth ?
									<>
										<NavLink activeclassname="active" to="/gallery" className="nav-link">MyGallery</NavLink>
										<button onClick={ Logout } className="nav-link mx-lg-2 mx-md-3 mx-1 btn btn-link btn-sm shadow-none border-0 text-danger">Logout</button>
										<div className="vr mx-2 d-none d-lg-inline"></div>
										<div className="navbar-text me-3 px-3">
											<span className="text-primary fw-bold"><i className="bi bi-person-circle"></i>&nbsp; { authPerson?.name ?? '' }</span>
										</div>
									</>
									:
									<>
										<NavLink activeclassname="active" to="/login" className="nav-link">Login</NavLink>
										<NavLink activeclassname="active" to="/register" className="nav-link">Register</NavLink>
									</>
							}
						</div>
					</div>
				</div>
			</nav>
		</>
	)
}

export default Navbar