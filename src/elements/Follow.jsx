import { useEffect, useState } from "react"
import { useSetRecoilState } from "recoil"
import axiosJWT from "../share/axiosJWT"
import { toastState } from "../state"

const FollowUser = ({ user, auth }) => {
	const setToast = useSetRecoilState(toastState)
	const [following, setFollowing] = useState(false)
	const checkFollow = async () => {
		const { data } = await axiosJWT.post(`/follows/check/${auth.id}/${user.id}`, {}, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		if (data.status) {
			setFollowing(false)
		} else {
			setFollowing(true)
		}
	}
	const follow_user = async () => {
		setFollowing(true)
		try {
			await axiosJWT.post(`/follows/follow/${auth.id}/${user.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(res => {
				const { data } = res
				if (data.status) {
					setFollowing(true)
				} else {
					setFollowing(false)
				}
			})
		} catch (error) {
			console.error(error)
			setFollowing(false)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const unfollow_user = async () => {
		setFollowing(false)
		try {
			await axiosJWT.post(`/follows/unfollow/${auth.id}/${user.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(res => {
				const { data } = res
				if (data.status) {
					setFollowing(false)
				} else {
					setFollowing(true)
				}
			})
		} catch (error) {
			console.error(error)
			setFollowing(true)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	useEffect(() => {
		(async () => {
			await checkFollow()
		})()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	return (
		<>
			{
				auth.id === user.id ?
					<></>
					:
					<>
						<button className={ `btn ${following ? `btn-secondary` : `btn-primary`} rounded-pill shadow` } onClick={ () => {
							following ? unfollow_user() : follow_user()
						} }>
							{
								following ?
									<>
										<i className="bi bi-person-dash me-2"></i>Unfollow
									</>
									:
									<>
										<i className="bi bi-person-add me-2"></i>Follow
									</>
							}
						</button>
					</>
			}
		</>
	)
}
export default FollowUser