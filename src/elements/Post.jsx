import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { useSetRecoilState } from "recoil"
import axiosJWT from "../share/axiosJWT"
import { toastState } from "../state"

const Post = ({ post, postBy, auth }) => {
	const setToast = useSetRecoilState(toastState)
	const [toDelete, setToDelete] = useState(false)
	const [deleted, setDeleted] = useState(false)
	const [like, setLike] = useState(false)
	const [likeCount, setLikeCount] = useState(0)
	const deletePost = async () => {
		try {
			const { data } = await axiosJWT.delete(`/posts/${post.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			setToast({
				type: data.type,
				message: data.message
			})
			if (data.status) {
				setDeleted(true)
			}
		} catch (error) {
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const countLike = async () => {
		try {
			const { data } = await axiosJWT.post(`/likes/count/${post.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			setLikeCount(data.count)
		} catch (error) {
			console.error(error)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const checkLike = async () => {
		try {
			const { data } = await axiosJWT.post(`/likes/check/${auth.id}/${post.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			if (data.status) {
				setLike(false)
			} else {
				setLike(true)
			}
		} catch (error) {
			console.error(error)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const likePost = async () => {
		setLike(true)
		setLikeCount(likeCount + 1)
		try {
			await axiosJWT.post(`/likes/like/${auth.id}/${post.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(res => {
				const { data } = res
				if (data.status) {
					setLike(true)
					countLike()
				} else {
					setLike(false)
				}
			})
		} catch (error) {
			console.error(error)
			setLike(false)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const dislikePost = async () => {
		setLike(false)
		setLikeCount(likeCount - 1)
		try {
			await axiosJWT.post(`/likes/dislike/${auth.id}/${post.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(res => {
				const { data } = res
				if (data.status) {
					setLike(false)
					countLike()
				} else {
					setLike(true)
				}
			})
		} catch (error) {
			setLike(true)
			console.error(error)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	useEffect(() => {
		(async () => {
			await countLike()
			await checkLike()
		})()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
	return (
		<>
			{
				deleted ?
					<></>
					:
					<>
						<div className="card border-0 rounded-4 shadow-sm mb-3 bg-blur postcard" key={ post.id }>
							<div className="card-header fw-bold px-4 py-3 border-0 bg-transparent rounded-4">
								<div className="row justify-content-between">
									<div className="col-10">
										<div className="w-100">
											<i className="bi bi-image-alt me-2 d-none"></i> { post.title }
										</div>
									</div>
									{
										auth.id === postBy.id ?
											<>
												<div className="col-auto">
													<div className="text-center h-100" role="button">
														<h6 className="text-warning my-auto">
															<i className="bi bi-pencil"></i>
														</h6>
													</div>
												</div>
											</>
											:
											<></>
									}
								</div>
							</div>
							<div className="card-body p-0">
								<div className="d-flex justify-content-center position-relative">
									<img src={ `/image/${post.photo}` } alt={ post.slug } loading="lazy" className="img-fluid" />
									<div className="position-absolute bottom-0 start-0">
										<Link to={ `/${postBy.username}` } className="badge text-bg-dark rounded-pill shadow-sm m-2 h-100 opacity-75 text-decoration-none">
											<span className="my-auto p-lg-1 fw-bold small">
												@{ postBy.username }
											</span>
										</Link>
									</div>
								</div>
								<div className="row justify-content-center my-3 my-lg-4">
									<div className="col mx-md-4 mx-3">
										<p className="text-muted small">
											{ post.caption }
										</p>
									</div>
								</div>
								<hr className="border border-danger border-1 bg-danger opacity-50" />
								<div className="row justify-content-between m-3 m-lg-4 h-100">
									<div className="col my-auto">
										<div className="row justify-content-start">
											<div className="col-auto mx-1">
												<div className="h-100 text-center" role="button" onClick={ () => {
													like ?
														dislikePost()
														:
														likePost()
												} }>
													<h3 className="text-primary my-auto">
														<i className={ `bi ${like ? `bi-heart-fill` : `bi-heart`}` }></i>
													</h3>
													<span className="text-primary small">{ likeCount }</span>
												</div>
											</div>
											<div className="col-auto mx-1">
												<div className="h-100 text-center" role="button">
													<h3 className="text-muted my-auto">
														<i className="bi bi-chat-right"></i>
													</h3>
													<span className="text-muted small">0</span>
												</div>
											</div>
										</div>
									</div>
									{
										auth.id === postBy.id ?
											<div className="col my-auto">
												<div className="row justify-content-end">
													{
														toDelete ?
															<div className="col-auto">
																<div className="btn-group btn-group-sm shadow-sm" role="group">
																	<button type="button" className="btn btn-danger small" onClick={ deletePost }>Yes, Delete</button>
																	<button type="button" className="btn btn-secondary small" onClick={ () => setToDelete(false) }>Cancel</button>
																</div>
															</div>
															:
															<div className="col-auto">
																<div role="button" className="h-100 text-center" onClick={ () => setToDelete(true) }>
																	<h4 className="text-danger my-auto opacity-75">
																		<i className="bi bi-trash"></i>
																	</h4>
																	<span className="small text-danger opacity-75">Delete</span>
																</div>
															</div>
													}
												</div>
											</div>
											:
											<></>
									}
								</div>
							</div>
						</div>
					</>
			}
		</>
	)
}
export default Post