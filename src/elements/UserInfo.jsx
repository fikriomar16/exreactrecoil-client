import { useEffect, useState } from "react"
import { useSetRecoilState } from "recoil"
import axiosJWT from "../share/axiosJWT"
import { toastState } from "../state"

const UserInfo = ({ user }) => {
	const setToast = useSetRecoilState(toastState)
	const [posts, setPosts] = useState(0)
	const [followers, setFollowers] = useState(0)
	const [following, setFollowing] = useState(0)
	const getPosts = async () => {
		try {
			const { data } = await axiosJWT.get(`/posts/count/${user.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			setPosts(data.count)
		} catch (error) {
			console.error(error)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const getFollowers = async () => {
		try {
			const { data } = await axiosJWT.get(`/follows/count/follower/${user.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			setFollowers(data.count)
		} catch (error) {
			console.error(error)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const getFollowing = async () => {
		try {
			const { data } = await axiosJWT.get(`/follows/count/following/${user.id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			setFollowing(data.count)
		} catch (error) {
			console.error(error)
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	useEffect(() => {
		(async () => {
			await getPosts()
			await getFollowers()
			await getFollowing()
		})()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	return (
		<>
			<div className="row justify-content-center text-center">
				<div className="col">
					<span className="d-block">Posts</span>
					<p className="h3">{ posts }</p>
				</div>
				<div className="vr p-0" />
				<div className="col">
					<span className="d-block">Followers</span>
					<p className="h3">{ followers }</p>
				</div>
				<div className="vr p-0" />
				<div className="col">
					<span className="d-block">Following</span>
					<p className="h3">{ following }</p>
				</div>
			</div>
		</>
	)
}
export default UserInfo