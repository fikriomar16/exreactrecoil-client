import { lazy, Suspense, useEffect } from "react"
import toast, { Toaster } from "react-hot-toast"
import { useRecoilValue } from "recoil"
import { toastState } from "../state"

const Navbar = lazy(() => import('../components/Navbar'))
const Footer = lazy(() => import('../components/Footer'))

const MainLayout = ({ children }) => {
	const toasting = useRecoilValue(toastState)
	useEffect(() => {
		if (toasting.type === 'success') toast.success(toasting.message)
		if (toasting.type === 'error') toast.error(toasting.message)
		if (toasting.type !== 'success' && toasting.type !== 'error' && toasting.message) {
			toast(toasting.message,
				{
					icon: '👏',
					style: {
						borderRadius: '10px',
						background: '#333',
						color: '#fff',
					},
				}
			)
		}
	}, [toasting])

	return (
		<>
			<Toaster position="top-center" reverseOrder={ false } />
			<div className="min-vh-100 bg-custom2">
				<Suspense fallback={ <><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></> }>
					<Navbar />
				</Suspense>
				<Suspense fallback={ <div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-border text-info" role="status"><span className="visually-hidden">Loading....</span></div></div> }>
					<div className="container py-4 py-lg-5 my-auto h-100">
						{ children }
					</div>
				</Suspense>
				<Suspense fallback={ <div className="d-flex justify-content-end m-3 m-lg-5"><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></div> }>
					<Footer />
				</Suspense>
			</div>
		</>
	)
}

export default MainLayout