import { lazy, Suspense } from "react"
import { Link } from "react-router-dom"
import logo from "../undraw/undraw_windows_re_uo4w.svg"

const FormInput = lazy(() => import('../components/FormInput'))

const ProcessUser = ({ errorList, form, roles, changeHandler, submitHandler, title, req = {}, theme, icon }) => {
	return (
		<>
			<div className="row justify-content-center h-100">
				<div className="col-md-5 col-3 my-auto h-100 py-2">
					<div className="d-flex justify-content-center mb-lg-4 mb-3">
						<img src={ logo } alt="logo" loading="lazy" className="img-fluid logo-shadow" height={ 320 } width={ 320 } />
					</div>
				</div>
				<div className="col-md-5 my-auto h-100">
					<div className="card rounded-4 border-0 shadow-sm">
						<div className={ `card-header border-0 bg-transparent px-5 pt-4 fw-bold text-${theme}` }>
							<i className={ `bi ${icon} me-3` }></i>&nbsp; { title }
						</div>
						<div className="card-body p-lg-4 p-3 shadow-sm">
							<form action="" onSubmit={ submitHandler }>
								<Suspense fallback={ <div className="spinner-border text-success text-center" role="status"><span className="visually-hidden">Loading....</span></div> }>
									<FormInput type="text" id="name" placeholder="Insert name here..." value={ form.name } onChange={ changeHandler } label="Name:" required={ true } />
									{ errorList.name && <span className="small text-danger px-3 mb-3">{ errorList.name.message }</span> }
									<FormInput type="text" id="username" placeholder="Insert username here..." value={ form.username } onChange={ changeHandler } label="Username:" required={ true } addClass={ 'mt-3' } />
									{ errorList.username && <span className="small text-danger px-3 mb-3">{ errorList.username.message }</span> }
									<FormInput type="email" id="email" placeholder="Insert email here..." value={ form.email } onChange={ changeHandler } label="Email:" required={ true } addClass={ 'mt-3' } />
									{ errorList.email && <span className="small text-danger px-3 mb-3">{ errorList.email.message }</span> }
									<FormInput type="password" id="password" placeholder="Insert password here..." value={ form.password } onChange={ changeHandler } label="Password:" required={ req.password ?? true } addClass={ 'mt-3' } />
									{ errorList.password && <span className="small text-danger px-3 mb-3">{ errorList.password.message }</span> }
									<div className="form-floating mt-3">
										<select required className="form-select rounded-4" id="roleId" aria-label="Floating label select role" onChange={ changeHandler } value={ form.roleId }>
											<option disabled value="">Select Role</option>
											{
												roles.map(role => (<option value={ +role.id } key={ role.id } selected={ (role.id === form.roleId) } > { role.name }</option>))
											}
										</select>
										<label htmlFor="roleId">Select Role for User</label>
									</div>
									{ errorList.roleId && <span className="small text-danger px-3 mb-3">{ errorList.roleId.message }</span> }
									<div className="d-flex justify-content-end mt-4">
										<button type="submit" className={ `btn btn-${theme} rounded-5 shadow` }>
											<i className={ `bi ${icon} fw-bold` }></i>&nbsp; { title }
										</button>
									</div>
								</Suspense>
							</form>
						</div>
						<div className="card-footer border-0 p-3">
							<div className="row justify-content-between">
								<div className="col">
									<Link to="/users" className="btn btn-danger rounded-5 shadow"><i className="bi bi-arrow-left"></i>&nbsp; Back</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default ProcessUser