import { lazy, Suspense } from "react"
import { useRecoilValue } from "recoil"
import { isAuth } from "../state"

const Landing = lazy(() => import('./Landing'))
const Welcome = lazy(() => import('./Welcome'))

const Home = () => {
	const checkAuth = useRecoilValue(isAuth)
	return (
		<>
			{
				checkAuth ?
					<>
						<Suspense fallback={ <><div className="alert alert-info"><div className="d-flex align-items-center"><strong>Loading...</strong><div className="spinner-border ms-auto"></div></div></div></> }>
							<Welcome />
						</Suspense>
					</>
					:
					<>
						<Suspense fallback={ <><div className="alert alert-primary"><div className="d-flex align-items-center"><strong>Loading...</strong><div className="spinner-border ms-auto"></div></div></div></> }>
							<Landing />
						</Suspense>
					</>
			}
		</>
	)
}
export default Home