import { Link } from "react-router-dom"
import logo_landing from "../undraw/undraw_dev_productivity_re_fylf.svg"
const Landing = () => {
	return (
		<>
			<div className="row justify-content-around h-100 mt-lg-3 mb-5">
				<div className="col-auto">
					<div className="row justify-content-center h-100 mb-5 mt-lg-3">
						<div className="col-auto my-auto h-100">
							<h1 className="fw-bold text-center mb-3">
								GALERIAsal
							</h1>
							<h5 className="fst-italic px-3 px-md-1">
								Express your Soul, Express your Free, Express your Joy..... <span className="small d-block"> (expressjs actually) </span>
							</h5>
							<h6 className="text-muted my-lg-5 mt-3 pt-3 text-center p-lg-5 small h-100">
								<i className="bi bi-info-square me-lg-2 me-1 my-auto"></i>&nbsp; Built with ExpresJS,ReactJS, & Recoil
							</h6>
						</div>
					</div>
				</div>
				<div className="col-lg-auto col-7 m-auto m-lg-0 h-100">
					<div className="d-flex justify-content-center mt-lg-5 pt-lg-3">
						<img src={ logo_landing } alt="logo" loading="lazy" className="img-fluid logo-shadow" height={ 300 } width={ 300 } />
					</div>
				</div>
			</div>
			<div className="row justify-content-start h-100 pt-3 pt-lg-0 m-2">
				<div className="col-lg-auto">
					<div className="row">
						<div className="col-auto">
							<p className="fst-italic h6 fw-bold">
								Find what interests you....
							</p>
							<Link to="/login" className="btn btn-primary rounded-pill fw-bold shadow mx-2">
								<i className="bi bi-arrow-right"></i>&nbsp; Getting Started
							</Link>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Landing