import { Link } from "react-router-dom"
import logoNotAuthorized from "../undraw/undraw_not_found_re_44w9.svg"

const NotAuthorized = () => {
	return (
		<>
			<div className="d-flex justify-content-center my-5 pt-4">
				<img src={ logoNotAuthorized } alt="logo" loading="lazy" className="img-fluid" height={ 300 } width={ 300 } />
			</div>
			<p className="my-2 text-center h5">
				Oops, You're not Authorized !!
			</p>
			<div className="d-flex justify-content-center my-4">
				<Link to="/" className="shadow fw-bold btn btn-dark text-decoration-none rounded-pill btn-lg">
					<i className="bi bi-arrow-left"></i>&nbsp; Go to Home
				</Link>
			</div>
		</>
	)
}
export default NotAuthorized