import { Link } from "react-router-dom"
import logo404 from "../undraw/undraw_page_not_found_re_e9o6.svg"

const NotFound = () => {
	return (
		<>
			<div className="d-flex justify-content-center my-5 pt-4">
				<img src={ logo404 } alt="logo" loading="lazy" className="img-fluid" height={ 300 } width={ 300 } />
			</div>
			<p className="my-2 text-center h5">
				Oops, Page Not Found
			</p>
			<div className="d-flex justify-content-center my-4">
				<Link to="/" className="shadow fw-bold btn btn-dark text-decoration-none rounded-pill btn-lg">
					<i className="bi bi-arrow-left"></i>&nbsp; Go to Home
				</Link>
			</div>
		</>
	)
}
export default NotFound