import { useNavigate } from "react-router-dom"
import { useRecoilValue } from "recoil"
import { authData } from "../state"
import logo_welcome from "../undraw/undraw_join_re_w1lh.svg"
const Welcome = () => {
	const authPerson = useRecoilValue(authData)
	const navigate = useNavigate()
	const goToGallery = () => navigate('/gallery')
	const goToExhibition = () => navigate('/exhibition')
	const goToSearch = () => navigate('/search')
	return (
		<>
			<div className="row justify-content-center">
				<div className="col-md-6">
					<div className="card rounded-4 shadow-sm border-0">
						<div className="card-header border-0 bg-transparent px-5 pt-4 fw-bold text-info">
							<i className="bi bi-house-door me-3"></i>&nbsp; Home
						</div>
						<div className="card-body p-lg-4 p-3">
							<div className="row justify-content-center mt-4">
								<div className="col">
									<div className="d-flex justify-content-center mb-lg-4 mb-3">
										<img src={ logo_welcome } alt="logo" loading="lazy" className="img-fluid logo-shadow" height={ 170 } width={ 170 } />
									</div>
									<h5 className="fw-bold text-center">Welcome, { authPerson.name }</h5>
									<div className="row justify-content-center mb-3">
										<div className="col-md-8 col-10">
											<ul className="list-group list-group-flush my-3 my-lg-4">
												<li className="list-group-item">Name: <span className="fw-bold">{ authPerson.name }</span></li>
												<li className="list-group-item">Username: <span className="fw-bold">{ authPerson.username }</span></li>
												<li className="list-group-item">Email: <span className="fw-bold">{ authPerson.email }</span></li>
												<li className="list-group-item">Role: <span className="fw-bold text-capitalize">{ authPerson.role }</span></li>
											</ul>
										</div>
									</div>
									<div className="row justify-content-center mb-3">
										<div className="col-auto">
											<button className="btn btn-outline-primary rounded-pill shadow fw-bold m-1" onClick={ goToExhibition }>
												<i className="bi bi-images me-2"></i> Visit Exhibition
											</button>
										</div>
										<div className="col-auto">
											<button className="btn btn-outline-info rounded-pill shadow fw-bold m-1" onClick={ goToGallery }>
												<i className="bi bi-file-image me-2"></i> MyGallery
											</button>
										</div>
										<div className="col-auto">
											<button className="btn btn-outline-success rounded-pill shadow fw-bold m-1" onClick={ goToSearch }>
												<i className="bi bi-search me-2"></i> Looking for someone
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Welcome