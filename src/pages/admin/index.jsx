import { lazy } from "react"
import { useRecoilValue } from "recoil"
import { authData } from "../../state"

const NotAuthorized = lazy(() => import('../NotAuthorized'))

const Admin = () => {
	const authPerson = useRecoilValue(authData)
	return (
		<>
			{
				authPerson.role.toLowerCase() === 'admin' ?
					<>
						<p>Silahkan</p>
					</>
					:
					<>
						<NotAuthorized />
					</>
			}
		</>
	)
}
export default Admin