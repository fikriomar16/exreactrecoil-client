import axios from "axios"
import { lazy, Suspense, useEffect, useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import { useRecoilState, useSetRecoilState } from "recoil"
import { authData, isAuth, toastState } from "../../state"
import jwt_decode from 'jwt-decode'
import logo_login from '../../undraw/undraw_login_re_4vu2.svg'

const FormInput = lazy(() => import('../../components/FormInput'))

const Login = () => {
	const [checkAuth, setCheckAuth] = useRecoilState(isAuth)
	const setAuthPerson = useSetRecoilState(authData)
	const setToast = useSetRecoilState(toastState)
	const navigate = useNavigate()
	useEffect(() => {
		if (checkAuth) navigate('/')
	}, [checkAuth, navigate])

	const [login, setLogin] = useState({
		'username': '',
		'password': ''
	})
	const [errorInput, setErrorInput] = useState([])
	const changeHandler = e => setLogin({ ...login, [e.target.id]: e.target.value })
	const submitHandler = async e => {
		e.preventDefault()
		try {
			await axios.post('/auth/login', login).then(async res => {
				const { data } = res
				if (res.status === 200) {
					if (data.length) {
						const tmpList = {}
						data.forEach(row => {
							tmpList[row.field] = row
						})
						setErrorInput(tmpList)
					} else {
						setErrorInput([])
						setCheckAuth(true)
						setAuthPerson(await jwt_decode(data.accessToken))
						localStorage.setItem('token', data.accessToken)
						setToast({
							type: 'success',
							message: 'Login Success'
						})
						navigate('/')
					}
				}
			})
		} catch (error) {
			setErrorInput([])
			setToast({
				type: 'error',
				message: error.response.data.error
			})
		}
	}
	return (
		<>
			<div className="row justify-content-center my-auto h-100 mt-lg-5 pt-lg-4">
				<div className="col-md-6 my-auto py-2">
					<div className="d-flex justify-content-center">
						<img src={ logo_login } alt="logo" loading="lazy" className="img-fluid logo-shadow" height={ 330 } width={ 330 } />
					</div>
				</div>
				<div className="col-md-4 my-auto py-2">
					<div className="card rounded-4 border-0 bg-transparent">
						<div className="card-body p-lg-4 p-3">
							<form action="" onSubmit={ submitHandler }>
								<Suspense fallback={ <div className="spinner-border text-primary" role="status"><span className="visually-hidden">Loading....</span></div> }>
									<FormInput type="text" id="username" placeholder="Insert username here..." value={ login.username } onChange={ changeHandler } label="Username:" required={ true } autoFocus={ true } />
									{ errorInput.username && <span className="small fw-bold text-danger px-3 mb-3">{ errorInput.username.message }</span> }
									<FormInput type="password" id="password" placeholder="Insert password here..." value={ login.password } onChange={ changeHandler } label="Password:" required={ true } addClass={ 'mt-3' } />
									{ errorInput.password && <span className="small fw-bold text-danger px-3 mb-3">{ errorInput.password.message }</span> }
									<div className="d-flex justify-content-end mt-4">
										<button type="submit" className="btn btn-primary rounded-5 shadow-sm"><i className="bi bi-box-arrow-in-right"></i>&nbsp; Login</button>
									</div>
								</Suspense>
							</form>
						</div>
						<div className="card-footer border-0 p-3 bg-transparent">
							<div className="row justify-content-between">
								<div className="col-auto">
									<span>Doesn't have any account? <Link to="/register" className="text-primary">Register</Link></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default Login