import axios from "axios"
import { lazy, Suspense, useEffect, useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import { useRecoilValue, useSetRecoilState } from "recoil"
import { isAuth, toastState } from "../../state"
import logo_register from '../../undraw/undraw_my_password_re_ydq7.svg'

const FormInput = lazy(() => import('../../components/FormInput'))

const Register = () => {
	const navigate = useNavigate()
	const setToast = useSetRecoilState(toastState)
	const checkAuth = useRecoilValue(isAuth)
	const [errorInput, setErrorInput] = useState([])
	const [register, setRegister] = useState({
		name: '',
		username: '',
		email: '',
		password: '',
		roleId: 4,
	})
	const changeHandler = e => setRegister({ ...register, [e.target.id]: e.target.value })
	const submitHandler = async e => {
		e.preventDefault()
		setErrorInput([])
		try {
			await axios.post('/auth/register', register).then(async res => {
				const { data, status } = res
				if (status === 200) {
					if (data.length) {
						const tmpList = {}
						data.forEach(row => {
							tmpList[row.field] = row
						})
						setErrorInput(tmpList)
					} else {
						if (data.status) {
							setToast({
								type: 'success',
								message: data.message
							})
							navigate('/login')
						} else {
							setToast({
								type: 'error',
								message: data.message ?? 'Error !!'
							})
						}
					}
				}
			})
		} catch (error) {
			setErrorInput([])
			console.error(error)
			setToast({
				type: 'error',
				message: error.response.data.error ?? error.response.data
			})
		}
	}
	useEffect(() => {
		if (checkAuth) navigate('/')
	}, [checkAuth, navigate])
	return (
		<>
			<div className="row justify-content-center my-auto h-100 mt-lg-4 pt-lg-3">
				<div className="col-md-5 col-3 my-auto py-2">
					<div className="d-flex justify-content-center">
						<img src={ logo_register } alt="logo" loading="lazy" className="img-fluid logo-shadow" height={ 330 } width={ 330 } />
					</div>
				</div>
				<div className="col-md-5 my-auto">
					<div className="card rounded-4 border-0 shadow-sm">
						<div className="card-header px-5 border-0 shadow-sm fw-bold">Register</div>
						<div className="card-body p-lg-4 p-3">
							<form action="" onSubmit={ submitHandler }>
								<Suspense fallback={ <div className="spinner-border text-primary" role="status"><span className="visually-hidden">Loading....</span></div> }>
									<FormInput type="text" id="name" placeholder="Insert name here..." value={ register.name } onChange={ changeHandler } label="Name:" required={ true } autoFocus={ true } />
									{ errorInput.name && <span className="small text-danger px-3 mb-3">{ errorInput.name.message }</span> }
									<FormInput type="text" id="username" placeholder="Insert username here..." value={ register.username } onChange={ changeHandler } label="Username:" required={ true } addClass={ 'mt-3' } />
									{ errorInput.username && <span className="small text-danger px-3 mb-3">{ errorInput.username.message }</span> }
									<FormInput type="email" id="email" placeholder="Insert email here..." value={ register.email } onChange={ changeHandler } label="Email:" required={ true } addClass={ 'mt-3' } />
									{ errorInput.email && <span className="small text-danger px-3 mb-3">{ errorInput.email.message }</span> }
									<FormInput type="password" id="password" placeholder="Insert password here..." value={ register.password } onChange={ changeHandler } label="Password:" required={ true } addClass={ 'mt-3' } />
									{ errorInput.password && <span className="small text-danger px-3 mb-3">{ errorInput.password.message }</span> }
									<div className="d-flex justify-content-end mt-4">
										<button type="submit" className="btn btn-primary rounded-5 shadow"><i className="bi bi-box-arrow-in-up"></i>&nbsp; Register</button>
									</div>
								</Suspense>
							</form>
						</div>
						<div className="card-footer border-0 p-3">
							<div className="row justify-content-end mx-4">
								<div className="col-auto">
									<span>Already have an account? <Link to="/login" className="text-primary">Login</Link></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Register