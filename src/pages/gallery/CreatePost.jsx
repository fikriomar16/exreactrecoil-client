import { lazy, Suspense, useState, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { useRecoilValue, useSetRecoilState } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { authData, toastState } from "../../state"

const FormInput = lazy(() => import('../../components/FormInput'))
const CreatePost = () => {
	const navigate = useNavigate()
	const authPerson = useRecoilValue(authData)
	const setToast = useSetRecoilState(toastState)
	const userId = authPerson.id
	const [create, setCreate] = useState({
		'title': '',
		'caption': ''
	})
	const changeHandler = e => setCreate({ ...create, [e.target.id]: e.target.value })
	const [photo, setPhoto] = useState(null)
	const [preview, setPreview] = useState('')
	const [selected, setSelected] = useState(false)
	const [errorInput, setErrorInput] = useState([])
	const onSelectFile = e => {
		if (!e.target.files || e.target.files.length === 0) {
			setSelected(undefined)
			return
		}
		setSelected(e.target.files[0])
	}
	useEffect(() => {
		if (!selected) {
			setPreview(undefined)
			return
		}
		const objectUrl = URL.createObjectURL(selected)
		setPreview(objectUrl)
		return () => URL.revokeObjectURL(objectUrl)
	}, [selected])
	const submitHandler = async e => {
		e.preventDefault()
		const formData = new FormData()
		formData.append('userId', +userId)
		formData.append('photo', photo[0])
		formData.append('title', create.title)
		formData.append('caption', create.caption)
		try {
			await axiosJWT.post('/posts', formData, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(async res => {
				const { data, status } = res
				if (status === 200) {
					if (data.status) {
						setToast({
							type: 'success',
							message: data.message
						})
						navigate('/gallery')
					} else {
						const tmpList = {}
						data.forEach(row => {
							tmpList[row.field] = row
						})
						setErrorInput(tmpList)
						setToast({
							type: 'error',
							message: data.message ?? 'Error !!'
						})
					}
				}
			})
		} catch (error) {
			setToast({
				type: 'error',
				message: error.response.data?.error ?? error.response.data?.message
			})
		}
	}
	return (
		<>
			<div className="d-flex justify-content-center mb-lg-5 mb-4">
				<h5 className="fw-bold">Create New Post</h5>
			</div>
			<div className="row justify-content-center my-lg-5 my-2 gap-5">
				<div className={ `col-lg-4 col-md-4 col-8 ${selected ? `` : `d-none`}` }>
					{
						selected &&
						<>
							<div className="d-flex justify-content-center mb-2">
								<img src={ preview } alt="preview" className="rounded img-fluid shadow-sm" loading="lazy" height={ 200 } width={ `auto` } />
							</div>
						</>
					}
				</div>
				<div className="col-lg-4 col-md-5 col-11">
					<div className="row justify-content-center">
						<div className="col">
							<form encType="multipart/form-data" onSubmit={ submitHandler }>
								<Suspense fallback={ <div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-primary" role="status"><span className="visually-hidden">Loading....</span></div></div> }>
									<div className="mb-3">
										<label htmlFor="photo" className="form-label px-4">Select Photo to Upload : </label>
										<input className="form-control rounded-3 shadow-sm" type="file" id="photo" accept="image/*" onChange={ (e) => {
											setPhoto(e.target.files)
											onSelectFile(e)
										} } required />
									</div>
									<FormInput type="text" id="title" placeholder="Insert title here..." value={ create.title } onChange={ changeHandler } label="Post Title : " required={ true } />
									{ errorInput.title && <span className="small text-danger px-3 mb-3">{ errorInput.title.message }</span> }
									<div className="form-floating mt-3">
										<textarea className="form-control rounded-4" placeholder="Leave a comment here" id="caption" style={ { height: '100px' } } value={ create.caption } onChange={ changeHandler } />
										<label htmlFor="caption">Say something....</label>
									</div>
									<div className="d-flex justify-content-center py-2 mt-4">
										<button type="submit" className="btn btn-dark rounded-pill shadow-sm"><i className="bi bi-send me-2"></i> Post Now!!</button>
									</div>
								</Suspense>
							</form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default CreatePost