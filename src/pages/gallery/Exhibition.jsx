import { lazy, Suspense, useEffect, useState } from "react"
import { useRecoilValue } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { authData } from "../../state"

const Post = lazy(() => import('../../elements/Post'))

const Exhibition = () => {
	const authPerson = useRecoilValue(authData)
	const [posts, setPosts] = useState([])
	const fetchData = async () => {
		const { data } = await axiosJWT.get(`/posts/for/${authPerson.id}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		setPosts(data)
	}
	useEffect(() => {
		fetchData()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	return (
		<>
			<div className="row justify-content-start">
				<Suspense fallack={ <><div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></div></> }>
					{
						posts.map(post => (
							<div className="col-md-6 col-lg-4" key={ post.id }>
								<Post post={ post } auth={ authPerson } postBy={ post.user } />
							</div>
						))
					}
				</Suspense>
			</div>
		</>
	)
}
export default Exhibition