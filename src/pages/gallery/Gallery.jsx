import { useState, useEffect, lazy, Suspense } from "react"
import { useNavigate } from "react-router-dom"
import { useRecoilValue } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { authData } from "../../state"

const Post = lazy(() => import('../../elements/Post'))
const UserInfo = lazy(() => import('../../elements/UserInfo'))

const Gallery = () => {
	const navigate = useNavigate()
	const authPerson = useRecoilValue(authData)
	const [posts, setPosts] = useState([])
	const goCreatePost = () => navigate('/gallery/create')
	const getMyPosts = async () => {
		const { data } = await axiosJWT.get(`/posts/by/${authPerson.id}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		setPosts(data)
	}
	useEffect(() => {
		getMyPosts()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	return (
		<>
			<div className="row justify-content-center mb-4">
				<div className="col-lg-4 px-4 px-lg-5 mb-3">
					<h6 className="fw-bold fst-italic text-primary opacity-75">@{ authPerson.username }</h6>
					<h3>{ authPerson.name }</h3>
				</div>
				<div className="col-lg-6">
					<Suspense fallack={ <><div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></div></> }>
						<UserInfo user={ authPerson } />
					</Suspense>
				</div>
			</div>
			<div className="row justify-content-center mb-3 mb-lg-5">
				<div className="col-auto">
					<button className="btn btn-dark rounded-pill shadow" onClick={ goCreatePost }> <i className="bi bi-file-richtext me-2"></i> Create New Post</button>
				</div>
			</div>
			<div className="row justify-content-start my-3">
				<Suspense fallack={ <><div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></div></> }>
					{
						posts.map(post => (
							<div className="col-md-6 col-lg-4" key={ post.id }>
								<Post post={ post } auth={ authPerson } postBy={ authPerson } />
							</div>
						))
					}
				</Suspense>
			</div>
		</>
	)
}
export default Gallery