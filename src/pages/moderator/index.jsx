import { lazy } from "react"
import { useRecoilValue } from "recoil"
import { authData } from "../../state"

const NotAuthorized = lazy(() => import('../NotAuthorized'))

const Moderator = () => {
	const authPerson = useRecoilValue(authData)
	return (
		<>
			{
				authPerson.role.toLowerCase() === 'moderator' ?
					<>
						<p>Silahkan</p>
					</>
					:
					<>
						<NotAuthorized />
					</>
			}
		</>
	)
}
export default Moderator