import { useState, useEffect } from "react"
import { Link } from "react-router-dom"
import axiosJWT from "../../share/axiosJWT"

const Roles = () => {
	const [roles, setRoles] = useState([])
	const [loading, setLoading] = useState(true)
	const getRoles = async () => {
		setLoading(true)
		const res = await axiosJWT.get('/roles', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		const { data } = res
		setRoles(data)
		setLoading(false)
	}
	useEffect(() => {
		getRoles()
	}, [])

	return (
		<>
			<div className="row justify-content-center">
				<div className="col-md-8">
					<div className="card rounded-4 border-0 shadow-sm">
						<div className="card-header border-0 bg-transparent px-5 pt-4 fw-bold text-primary">
							<i className="bi bi-signpost-2 me-2"></i>&nbsp; Roles Data
						</div>
						<div className="card-body p-lg-4 p-3">
							<div className="row justify-content-center">
								<div className="col">
									<div className="table-responsive overflow-visible">
										<table className="table table-hover align-middle table-striped">
											<thead className="table-light">
												<tr>
													<th className="text-center">No.</th>
													<th>Role</th>
													<th className="text-end">Action</th>
												</tr>
											</thead>
											<tbody>
												{
													roles.map((role, idx) => (
														<tr key={ role.id }>
															<td className="text-center">{ (idx + 1) }.</td>
															<td>{ role.name }</td>
															<td className="text-end">
																<div className="btn-group dropup">
																	<button type="button" className="btn btn-outline-dark btn-sm border-0 rounded-3" data-bs-toggle="dropdown" aria-expanded="false">
																		<i className="bi bi-three-dots-vertical"></i>
																	</button>
																	<ul className="dropdown-menu border-0 shadow">
																		<li><Link className="dropdown-item" to="/">Detail</Link></li>
																		<li><Link className="dropdown-item" to="/">Edit</Link></li>
																		<li><button className="dropdown-item" onClick={ () => console.log('alert') }>Delete</button></li>
																	</ul>
																</div>
															</td>
														</tr>
													))
												}
											</tbody>
										</table>
									</div>
									{
										loading &&
										<>
											<div className="alert alert-primary">
												<div className="d-flex align-items-center">
													<strong>Loading...</strong>
													<div className="spinner-border ms-auto" />
												</div>
											</div>
										</>
									}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Roles