import { lazy } from "react"
import { useRecoilValue } from "recoil"
import { authData } from "../../state"

const NotAuthorized = lazy(() => import('../NotAuthorized'))

const Super = () => {
	const authPerson = useRecoilValue(authData)
	return (
		<>
			{
				authPerson.role.toLowerCase() === 'super' ?
					<>
						<p>Silahkan</p>
					</>
					:
					<>
						<NotAuthorized />
					</>
			}
		</>
	)
}
export default Super