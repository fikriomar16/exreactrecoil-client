import { lazy, Suspense, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { useSetRecoilState } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { toastState } from "../../state"

const ProcessUser = lazy(() => import('../../layouts/ProcessUser'))
const AddUser = () => {
	const navigate = useNavigate()
	const [create, setCreate] = useState({
		name: '',
		username: '',
		email: '',
		password: '',
		roleId: '',
	})
	const theme = 'primary'
	const icon = 'bi-save'
	const [roles, setRoles] = useState([])
	const [errorInput, setErrorInput] = useState([])
	const setToast = useSetRecoilState(toastState)
	const changeHandler = e => setCreate({ ...create, [e.target.id]: e.target.value })
	const submitHandler = async e => {
		setErrorInput([])
		e.preventDefault()
		try {
			await axiosJWT.post('/users', create, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(async res => {
				const { data, status } = res
				if (status === 200) {
					if (data.length) {
						const tmpList = {}
						data.forEach(row => {
							tmpList[row.field] = row
						})
						setErrorInput(tmpList)
					} else {
						if (data.status) {
							setToast({
								type: 'success',
								message: data.message
							})
							navigate('/users')
						} else {
							setToast({
								type: 'error',
								message: data.message ?? 'Error !!'
							})
						}
					}
				}
			})
		} catch (error) {
			setToast({
				type: 'error',
				message: error.response.data?.error ?? error.response.data
			})
		}
	}
	const getRoles = async () => {
		const { data } = await axiosJWT.get('/roles', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		setRoles(data)
	}
	useEffect(() => {
		getRoles()
	}, [])
	return (
		<>
			<Suspense fallback={ <div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-primary" role="status"><span className="visually-hidden">Loading....</span></div></div> }>
				<ProcessUser roles={ roles } changeHandler={ changeHandler } form={ create } submitHandler={ submitHandler } errorList={ errorInput } title={ `Add New User` } theme={ theme } icon={ icon } />
			</Suspense>
		</>
	)
}
export default AddUser