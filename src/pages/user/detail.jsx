import { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { useSetRecoilState } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { toastState } from "../../state"
import logo_detail from "../../undraw/undraw_articles_wbpb.svg"

const DetailUser = () => {
	const navigate = useNavigate()
	const { id } = useParams()
	const setToast = useSetRecoilState(toastState)
	const [user, setUser] = useState({})
	const backHandler = () => navigate(-1)
	const getUser = async () => {
		const { data } = await axiosJWT.get(`/users/${id}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		if (data) {
			const { name, username, email, roleId } = data
			const role = data.role.name
			setUser({ name, username, email, roleId, role })
		} else {
			setToast({
				type: 'error',
				message: 'User Not Found !!'
			})
			navigate('/users')
		}
	}
	useEffect(() => {
		getUser()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
	return (
		<>
			<div className="row justify-content-center">
				<div className="col-md-4">
					<div className="card rounded-4 shadow-sm border-0">
						<div className="card-header border-0 bg-transparent px-5 pt-4 fw-bold text-info">
							<i className="bi bi-body-text me-3"></i>&nbsp; Detail
						</div>
						<div className="card-body p-lg-4 p-3">
							<div className="row justify-content-center mt-4">
								<div className="col">
									<div className="d-flex justify-content-center mb-lg-4 mb-3">
										<img src={ logo_detail } alt="logo" loading="lazy" className="img-fluid logo-shadow" height={ 150 } width={ 150 } />
									</div>
									<div className="row justify-content-center">
										<div className="col-lg-10">
											<ul className="list-group my-3 my-lg-4">
												<li className="list-group-item">Name: <span className="fw-bold">{ user.name }</span></li>
												<li className="list-group-item">Username: <span className="fw-bold">{ user.username }</span></li>
												<li className="list-group-item">Email: <span className="fw-bold">{ user.email }</span></li>
												<li className="list-group-item">Role: <span className="fw-bold text-capitalize">{ user.role }</span></li>
											</ul>
										</div>
									</div>
									<div className="d-flex justify-content-center my-2">
										<button className="btn btn-secondary rounded-pill shadow-sm fw-bold" onClick={ backHandler }>
											<i className="bi bi-arrow-left"></i>&nbsp; Back
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default DetailUser