import { lazy, Suspense, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { useSetRecoilState } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { toastState } from "../../state"

const ProcessUser = lazy(() => import('../../layouts/ProcessUser'))

const EditUser = () => {
	const navigate = useNavigate()
	const { id } = useParams()
	const setToast = useSetRecoilState(toastState)
	const [roles, setRoles] = useState([])
	const [errorInput, setErrorInput] = useState([])
	const [update, setUpdate] = useState({
		name: '',
		username: '',
		email: '',
		password: '',
		roleId: '',
	})
	const theme = 'warning'
	const icon = 'bi-save2'
	const changeHandler = e => setUpdate({ ...update, [e.target.id]: e.target.value })
	const submitHandler = async e => {
		setErrorInput([])
		e.preventDefault()
		try {
			await axiosJWT.put(`/users/${id}`, update, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}).then(async res => {
				const { data, status } = res
				if (status === 200) {
					if (data.length) {
						const tmpList = {}
						data.forEach(row => {
							tmpList[row.field] = row
						})
						setErrorInput(tmpList)
					} else {
						if (data.status) {
							setToast({
								type: 'success',
								message: data.message
							})
							navigate('/users')
						} else {
							setToast({
								type: 'error',
								message: data.message ?? 'Error !!'
							})
						}
					}
				}
			})
		} catch (error) {
			setToast({
				type: 'error',
				message: error.response.data?.error ?? error.response.data
			})
		}
	}
	const getRoles = async () => {
		const { data } = await axiosJWT.get('/roles', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		setRoles(data)
	}
	const getUser = async () => {
		const { data } = await axiosJWT.get(`/users/${id}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		if (!data) {
			setToast({
				type: 'error',
				message: 'User Not Found !!'
			})
			navigate('/users')
		}
		const { name, username, email } = data
		const roleId = String(data.roleId)
		setUpdate({ name, username, email, roleId, password: '' })
	}
	useEffect(() => {
		getRoles()
		getUser()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
	return (
		<>
			<Suspense fallback={ <div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-warning" role="status"><span className="visually-hidden">Loading....</span></div></div> }>
				<ProcessUser roles={ roles } changeHandler={ changeHandler } form={ update } submitHandler={ submitHandler } errorList={ errorInput } title={ `Edit User` } req={ { password: false } } theme={ theme } icon={ icon } />
			</Suspense>
		</>
	)
}
export default EditUser