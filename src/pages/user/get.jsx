import { lazy, Suspense, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { useRecoilValue } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { authData } from "../../state"
import logo_detail from "../../undraw/undraw_articles_wbpb.svg"

const Post = lazy(() => import('../../elements/Post'))
const Follow = lazy(() => import('../../elements/Follow'))
const UserInfo = lazy(() => import('../../elements/UserInfo'))

const GetUser = () => {
	const { username } = useParams()
	const authPerson = useRecoilValue(authData)
	const [user, setUser] = useState({})
	const [posts, setPosts] = useState([])
	const getUser = async () => {
		const { data } = await axiosJWT.get(`/users/show/${username}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		setUser(data)
		setPosts(data.posts)
	}
	useEffect(() => {
		getUser()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
	return (
		<>
			<div className="row justify-content-start">
				<div className="col-lg-3 col-md-4 mb-4 mb-lg-2">
					<div className="card rounded-4 shadow-sm border-0">
						<div className="card-header border-0 bg-transparent px-5 pt-4 fw-bold text-info">
							<i className="bi bi-person-bounding-box me-3"></i>&nbsp; Profile
						</div>
						<div className="card-body p-lg-4 p-3">
							<div className="row justify-content-center mt-4">
								<div className="col">
									<div className="d-flex justify-content-center mb-lg-4 mb-3">
										<img src={ logo_detail } alt="logo" loading="lazy" className="img-fluid logo-shadow" height={ 150 } width={ 150 } />
									</div>
									<div className="row justify-content-center px-4 px-lg-1">
										<div className="col">
											<ul className="list-group my-3 my-lg-4">
												<li className="list-group-item">Username: <span className="fw-bold">&nbsp;@{ user.username }</span></li>
												<li className="list-group-item">Name: <span className="fw-bold">&nbsp;{ user.name }</span></li>
											</ul>
										</div>
									</div>
									<div className="row mb-3">
										<div className="col">
											<Suspense fallack={ <><div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></div></> }>
												{ user.id ? <UserInfo user={ user } /> : <></> }
											</Suspense>
										</div>
									</div>
									<div className="d-flex justify-content-center my-2">
										<Suspense fallack={ <><div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></div></> }>
											{ user.id ? <Follow user={ user } auth={ authPerson } /> : <></> }
										</Suspense>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-md-8">
					<div className="row justify-content-start">
						<Suspense fallack={ <><div className="d-flex justify-content-center m-3 m-lg-5"><div className="spinner-grow text-info" role="status"><span className="visually-hidden">Loading....</span></div></div></> }>
							{
								posts.map(post => (
									<div className="col-md-6 col-lg-4" key={ post.id }>
										<Post post={ post } auth={ authPerson } postBy={ user } />
									</div>
								))
							}
						</Suspense>
					</div>
				</div>
			</div>
		</>
	)
}
export default GetUser