import { useState, useEffect } from "react"
import { Link, useNavigate } from "react-router-dom"
import { useRecoilValue, useSetRecoilState } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { authData, isAuth, toastState } from "../../state"

const Users = () => {
	const checkAuth = useRecoilValue(isAuth)
	const authPerson = useRecoilValue(authData)
	const setToast = useSetRecoilState(toastState)
	const navigate = useNavigate()
	const [users, setUsers] = useState([])
	const [toDelete, setToDelete] = useState(0)
	const [loading, setLoading] = useState(true)
	const getUsers = async () => {
		setLoading(true)
		try {
			const { data } = await axiosJWT.get('/users', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			setLoading(false)
			setUsers(data)
		} catch (error) {
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	const deleteUser = async id => {
		try {
			const { data } = await axiosJWT.delete(`/users/${id}`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			setToast({
				type: data.type,
				message: data.message
			})
			if (data.status) {
				getUsers()
			}
		} catch (error) {
			setToast({
				type: 'error',
				message: error.response.data ?? error.message
			})
		}
	}
	useEffect(() => {
		if (checkAuth) {
			getUsers()
		} else {
			navigate('/login')
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
	return (
		<>
			<div className="row justify-content-center">
				<div className="col-md-8">
					<div className="card rounded-4 border-0 shadow-sm">
						<div className="card-header border-0 bg-transparent px-5 pt-4 fw-bold text-primary">
							<i className="bi bi-people me-2"></i>&nbsp; Users Data
						</div>
						<div className="card-body p-lg-4 p-3">
							{
								(authPerson.role.toLowerCase() === 'super' || authPerson.role.toLowerCase() === 'admin') &&
								<>
									<div className="row justify-content-end mt-2 mb-4">
										<div className="col-auto">
											<Link to="/users/add" className="btn btn-sm btn-success rounded-pill shadow fw-bold">
												<i className="bi bi-plus-lg"></i>&nbsp; Add User
											</Link>
										</div>
									</div>
								</>
							}
							<div className="row justify-content-center">
								<div className="col">
									<div className="table-responsive">
										<table className="table table-hover align-middle table-striped">
											<thead className="table-light">
												<tr>
													<th className="text-center">No.</th>
													<th>Username</th>
													<th>Name</th>
													<th>Email</th>
													<th>Role</th>
													<th className="text-center">Action</th>
												</tr>
											</thead>
											<tbody>
												{
													users.map((user, idx) => (
														<tr key={ user.id }>
															<td className="text-center">{ (idx + 1) }.</td>
															<td>{ user.username }</td>
															<td>{ user.name }</td>
															<td>{ user.email }</td>
															<td>{ user.role.name }</td>
															<td className="text-center">
																{
																	toDelete === user.id ?
																		<>
																			<span className="text-muted d-flex justify-content-center">Delete this row?</span>
																			<div className="row justify-content-center px-0 mx-0">
																				<div className="col-auto">
																					<button className="btn btn-sm btn-danger shadow-sm m-1 m-lg-2" onClick={ () => deleteUser(user.id) }>Yes, Delete</button>
																				</div>
																				<div className="col-auto">
																					<button className="btn btn-sm btn-secondary shadow-sm m-1 m-lg-2" onClick={ () => setToDelete(0) }>Cancel</button>
																				</div>
																			</div>
																		</>
																		:
																		<>
																			<div className="row justify-content-center px-0 mx-0">
																				<div className="col-auto my-1">
																					<Link to={ `/users/${user.id}` } className="btn btn-sm btn-info fw-bold shadow-sm"><i className="bi bi-body-text"></i>&nbsp; Detail</Link>
																				</div>
																				{
																					(authPerson.role.toLowerCase() === 'super' || authPerson.role.toLowerCase() === 'admin') &&
																					<>
																						<div className="col-auto my-1">
																							<Link to={ `/users/${user.id}/edit` } className="btn btn-sm btn-warning fw-bold shadow-sm"><i className="bi bi-pencil-square"></i>&nbsp; Edit</Link>
																						</div>
																						<div className="col-auto my-1">
																							<button className="btn btn-sm btn-danger fw-bold shadow-sm" onClick={ () => setToDelete(user.id) }><i className="bi bi-trash"></i>&nbsp; Delete</button>
																						</div>
																					</>
																				}
																			</div>
																		</>
																}
															</td>
														</tr>
													))
												}
											</tbody>
										</table>
									</div>
									{
										loading &&
										<>
											<div className="alert alert-primary">
												<div className="d-flex align-items-center">
													<strong>Loading...</strong>
													<div className="spinner-border ms-auto" />
												</div>
											</div>
										</>
									}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Users