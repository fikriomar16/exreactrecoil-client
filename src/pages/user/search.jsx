import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { useRecoilValue } from "recoil"
import axiosJWT from "../../share/axiosJWT"
import { authData } from "../../state"
import logo_search from "../../undraw/undraw_handcrafts_search_files.svg"

const SearchUser = () => {
	const [toSearch, setToSearch] = useState({
		search: '',
		limit: 20
	})
	const [lists, setLists] = useState([])
	const authPerson = useRecoilValue(authData)
	const changeHandler = e => setToSearch({ ...toSearch, [e.target.id]: e.target.value })
	const goSearch = async () => {
		const { data } = await axiosJWT.post(`/users/search?search=${toSearch.search}&limit=${toSearch.limit}`, {
			except: authPerson.id
		}, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		setLists(data.result)
	}
	useEffect(() => {
		if (toSearch.search) {
			goSearch()
		} else {
			setLists([])
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [toSearch])
	return (
		<>
			<div className="row justify-content-center">
				<div className="col">
					<div className="row justify-content-center mb-md-3">
						<div className="col-md-4 col-10">
							<div className="input-group flex-nowrap">
								<span className="input-group-text bg-primary text-light opacity-75 border-primary fw-bold small" id="search-person">@</span>
								<input type="text" className="form-control" id="search" placeholder="Type username or name here" aria-label="Search" aria-describedby="search-person" onChange={ changeHandler } value={ toSearch.search } autoFocus={ true } />
							</div>
						</div>
					</div>
					<div className="row justify-content-center h-100 mb-3">
						<div className="col-2 my-auto">
							<div className="d-none d-md-inline">
								{
									lists.length ?
										<img src={ logo_search } alt="search icon" className="img-fluid logo-shadow" height={ 80 } width={ 80 } />
										:
										<></>
								}
							</div>
						</div>
						<div className="col-lg-4 col-md-5 my-auto">
							<div className="row justify-content-center">
								<div className="col">
									<div className="list-group list-group-flush">
										{
											lists.map(list => (
												<Link to={ `/${list.username}` } className="list-group-item list-group-item-action bg-transparent" key={ list.username }>
													<div className="row justify-content-start h-100 px-3">
														<div className="col-auto my-auto h-100">
															<h1 className="my-auto">
																<i className="bi bi-person-circle me-lg-2 me-1 text-primary opacity-75"></i>
															</h1>
														</div>
														<div className="col-auto">
															<h6 className="d-block text-primary fw-bold">@{ list.username }</h6>
															<h5 className="d-block">{ list.name }</h5>
														</div>
													</div>
												</Link>
											))
										}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default SearchUser