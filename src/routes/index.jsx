import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { lazy, Suspense } from 'react'
import MainLayout from '../layouts/MainLayout'
const Login = lazy(() => import('../pages/auth/Login'))
const Register = lazy(() => import('../pages/auth/Register'))
const NotFound = lazy(() => import('../pages/NotFound'))
const Home = lazy(() => import('../pages/Home'))
const Roles = lazy(() => import('../pages/role'))
const Users = lazy(() => import('../pages/user'))
const AddUser = lazy(() => import('../pages/user/add'))
const EditUser = lazy(() => import('../pages/user/edit'))
const DetailUser = lazy(() => import('../pages/user/detail'))
const Super = lazy(() => import('../pages/super'))
const Admin = lazy(() => import('../pages/admin'))
const Moderator = lazy(() => import('../pages/moderator'))
const Gallery = lazy(() => import('../pages/gallery/Gallery'))
const CreatePost = lazy(() => import('../pages/gallery/CreatePost'))
const SearchUser = lazy(() => import('../pages/user/search'))
const GetUser = lazy(() => import('../pages/user/get'))
const Exhibition = lazy(() => import('../pages/gallery/Exhibition'))

const Router = () => {
	return (
		<BrowserRouter>
			<Suspense fallback={ <div className="spinner-border text-danger" role="status"><span className="visually-hidden">Loading....</span></div> }>
				<Routes>
					<Route exact path="/" element={ <MainLayout children={ <Home /> } /> } />
					<Route exact path="/super" element={ <MainLayout children={ <Super /> } /> } />
					<Route exact path="/admin" element={ <MainLayout children={ <Admin /> } /> } />
					<Route exact path="/moderator" element={ <MainLayout children={ <Moderator /> } /> } />
					<Route exact path="/regular" element={ <MainLayout /> } />
					<Route exact path="/roles" element={ <MainLayout children={ <Roles /> } /> } />
					<Route exact path="/users" element={ <MainLayout children={ <Users /> } /> } />
					<Route exact path="/users/add" element={ <MainLayout children={ <AddUser /> } /> } />
					<Route exact path="/users/:id" element={ <MainLayout children={ <DetailUser /> } /> } />
					<Route exact path="/users/:id/edit" element={ <MainLayout children={ <EditUser /> } /> } />
					<Route exact path="/login" element={ <MainLayout children={ <Login /> } /> } />
					<Route exact path="/register" element={ <MainLayout children={ <Register /> } /> } />

					<Route exact path="/gallery" element={ <MainLayout children={ <Gallery /> } /> } />
					<Route exact path="/gallery/create" element={ <MainLayout children={ <CreatePost /> } /> } />
					<Route exact path="/search" element={ <MainLayout children={ <SearchUser /> } /> } />
					<Route exact path="/:username" element={ <MainLayout children={ <GetUser /> } /> } />
					<Route exact path="/exhibition" element={ <MainLayout children={ <Exhibition /> } /> } />
					<Route path="*" element={ <MainLayout children={ <NotFound /> } /> } />
				</Routes>
			</Suspense>
		</BrowserRouter>
	)
}

export default Router