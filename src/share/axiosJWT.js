import axios from "axios"
import jwt_decode from "jwt-decode"

const authPerson = jwt_decode(localStorage.getItem('token'))
const { exp } = authPerson
const axiosJWT = axios.create()

axiosJWT.interceptors.request.use(async (config) => {
	const currentDate = new Date()
	if (exp * 1000 < currentDate.getTime()) {
		const { data } = await axios.post('/token/refresh')
		config.headers.Authorization = `Bearer ${data.accessToken}`
		localStorage.setItem('token', data.accessToken)
	}
	return config
}, (error) => {
	return Promise.reject(error)
})

export default axiosJWT