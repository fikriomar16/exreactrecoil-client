import { atom } from 'recoil'
import { recoilPersist } from 'recoil-persist'

const { persistAtom } = recoilPersist()

export const isAuth = atom({
	key: 'isAuth',
	default: false,
	effects_UNSTABLE: [persistAtom]
})

export const authData = atom({
	key: 'authData',
	default: null,
	effects_UNSTABLE: [persistAtom]
})

export const toastState = atom({
	key: 'toastState',
	default: {}
})

